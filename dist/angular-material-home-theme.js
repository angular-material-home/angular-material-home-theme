/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/*
 * 
 */
angular.module('ngMaterialHomeTheme', [
	'ngMaterialHome'
]);
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeTheme')
/*
 * State machine of the system
 * 
 */
.config(function($routeProvider, $locationProvider) {
	$routeProvider //
	.otherwise({
		redirectTo : '/'
	})
	/***************************************************************************
	 * Settings
	 **************************************************************************/
//	/**
//	 * @ngdoc ngRoute
//	 * @name /preferences/:page
//	 * @description Preferences page
//	 * 
//	 * Adds a preference page to set theme of SPA.
//	 */
//	.when('/preferences/theme', {
//		templateUrl : 'views/amh-setting/themes.html',
//		controller: 'AmhThemeCtrl'
//	})//
	.when('/preferences/theme/manager', {
		templateUrl : 'views/amh-setting/theme-manager.html',
		controller : 'AmhThemeCtrl',
                                
                /*
                 * @ngInject
                 */
                protect : function($rootScope) {
                    return !$rootScope.app.user.tenant_owner; 
                },
                helpId:'preferences-theme-manager'
	});
	$locationProvider.html5Mode(true);
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeTheme')
/*
 * تعیین تم
 */
.config(function($mdThemingProvider) {

//	$mdThemingProvider.generateThemesOnDemand(true);
	$mdThemingProvider.alwaysWatchTheme(true);

	var gPalette = {
			'50' : 'e2f0f4',
			'100' : 'b7d8e3',
			'200' : '88bfd0',
			'300' : '58a5bd',
			'400' : '3491af',
			'500' : '107ea1',
			'600' : '0e7699',
			'700' : '0c6b8f',
			'800' : '096185',
			'900' : '054e74',
			'A100' : 'a3dbff',
			'A200' : '70c7ff',
			'A400' : '3db3ff',
			'A700' : '24a9ff',
			'contrastDefaultColor' : 'light',
			'contrastDarkColors' : [
				'50',
				'100',
				'200',
				'300',
				'A100',
				'A200',
				'A400',
				'A700'
				],
				'contrastLightColors' : [
					'400',
					'500',
					'600',
					'700',
					'800',
					'900'
					]
	};

	var aPalette = {
			'50' : 'fce5e0',
			'100' : 'f8bfb3',
			'200' : 'f49580',
			'300' : 'ef6a4d',
			'400' : 'eb4a26',
			'500' : 'e82a00',
			'600' : 'e52500',
			'700' : 'e21f00',
			'800' : 'de1900',
			'900' : 'd80f00',
			'A100' : 'ffffff',
			'A200' : 'eb4a26',
			'A400' : 'e52500',
			'A700' : 'd80f00',
			'contrastDefaultColor' : 'light',
			'contrastDarkColors' : [
				'50',
				'100',
				'200',
				'300'
				],
				'contrastLightColors' : [
					'400',
					'500',
					'600',
					'700',
					'800',
					'900',
					'A100',
					'A200',
					'A400',
					'A700'
					]
	};

	var bPalette = {
			'50' : 'fdfeff',
			'100' : 'fbfdfe',
			'200' : 'f8fbfe',
			'300' : 'f5f9fe',
			'400' : 'f2f8fd',
			'500' : 'f0f7fd',
			'600' : 'd0e0e3',
			'700' : 'a2c4c9',
			'800' : '76a5af',
			'900' : '45818e',
			'A100' : 'ffffff',
			'A200' : 'ffffff',
			'A400' : 'ffffff',
			'A700' : 'ffffff',
			'contrastDefaultColor' : 'dark',
			'contrastDarkColors' : [
				'50',
				'100',
				'200',
				'300',
				'400',
				'500',
				'600',
				'700',
				'A100',
				'A200',
				'A400',
				'A700'
				],
				'contrastLightColors' : [
					'800',
					'900'
					]
	};

	$mdThemingProvider.definePalette('hitechPrimary', gPalette);
	$mdThemingProvider.definePalette('hitechAccent', aPalette);
	$mdThemingProvider.definePalette('hitechBackground', bPalette);

	$mdThemingProvider.theme('hitech').primaryPalette('hitechPrimary');
	$mdThemingProvider.theme('hitech').accentPalette('hitechAccent');
	$mdThemingProvider.theme('hitech').backgroundPalette('hitechBackground');

	var pPalette = {
		  '50': 'f0e0e0',
		  '100': 'd9b3b3',
		  '200': 'c08080',
		  '300': 'a74d4d',
		  '400': '942626',
		  '500': '810000',
		  '600': '790000',
		  '700': '6e0000',
		  '800': '640000',
		  '900': '510000',
		  'A100': 'ff8484',
		  'A200': 'ff5151',
		  'A400': 'ff1e1e',
		  'A700': 'ff0404',
		  'contrastDefaultColor': 'light',
		  'contrastDarkColors': [
		    '50',
		    '100',
		    '200',
		    'A100',
		    'A200'
		  ],
		  'contrastLightColors': [
		    '300',
		    '400',
		    '500',
		    '600',
		    '700',
		    '800',
		    '900',
		    'A400',
		    'A700'
		  ]
		};
	
	$mdThemingProvider.definePalette('petroPrimary', pPalette);
	
	$mdThemingProvider.theme('petronomics').primaryPalette('petroPrimary');
	$mdThemingProvider.theme('petronomics').accentPalette('indigo');
	
	
//	$mdThemingProvider.setDefaultTheme('hitech');

});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeTheme')

/**
 * @ngdoc controller
 * @name AmhThemeCtrl
 * @description Theme setting controller
 * 
 * This controller is used to set theme for SPA
 */
.controller('AmhThemeCtrl', function ($scope, $mdTheming, $rootScope, $http, $theme){

	$scope.themes = $mdTheming.THEMES;
	$scope.myTheme = '';
	
	$http.get('resources/theme-preview.json')//
	.then(function(res){
		$scope.ctrl.template = res.data;
	});
	
	/**
	 * Adds new theme to app configuration
	 * 
	 * @memberof AmhThemeCtrl
	 * @return {promise} of added theme
	 */
	function addTheme(){
		return $theme.newTheme({
			name: 'theme' + Math.floor((Math.random() * 10000) + 1),
			title: 'Theme title',
			options: {}
		});
	}
	
	/**
	 * Remove theme form application
	 * 
	 * @memberof AmhThemeCtrl
	 * @param {object} theme - The theme
	 * @return {promise} to delete theme
	 */
	function deleteTheme(theme){
		return $theme.deleteTheme(theme)
		.then(function(){
			if(angular.equals($scope.selectedTheme, theme)){
				$scope.selectedTheme = null;
			}
		});
	}
	
	/**
	 * Select theme
	 * 
	 * @memberof AmhThemeCtrl
	 * @param {object} theme - The theme
	 */
	function selectTheme(theme){
		$scope.selectedTheme = theme;
		if(!theme.options){
			theme.options = [];
		}
	}
	
	// Watch on list of themes in config
	$scope.$watch(function(){
		return $rootScope.app.config.themes;
	}, function(){
		$rootScope.themes = [];
		// add customer themes
		if($rootScope.app.config.themes){			
			$rootScope.app.config.themes.forEach(function(theme){
				registerTheme(theme);
			});
		}
		$scope.themes = $mdTheming.THEMES;
	}, true);
	
	// Watch on myTheme variable
	$scope.$watch(function(){
		return $scope.myTheme;
	}, function(themeName){
		if(themeName && $mdTheming.registered(themeName)){
			$mdTheming.generateTheme(themeName);
		}
	});
	
	function registerTheme(theme){
		normalize(theme);
		$mdTheming.defineTheme(theme.name, theme.options);
		//reload the theme
//		$mdTheming.generateTheme(theme.name);
	}
	
	function normalize(theme){
		// check options
		if(typeof theme.options === 'undefined'){
			theme.options = {};
		}else{
			if(!theme.options.primary || theme.options.primary === ''){
				delete theme.options.primary;
			}
			if(!theme.options.accent || theme.options.accent === ''){
				delete theme.options.accent;
			}
			if(!theme.options.warn || theme.options.warn === ''){
				delete theme.options.warn;
			}
			if(!theme.options.background || theme.options.background === ''){
				delete theme.options.background;
			}
		}
		// check name
		if(typeof theme.name === 'undefined'){
			theme.name = 'theme' + Math.floor((Math.random() * 10000) + 1)
		}
	}
	
	function applyTheme(theme){
		$rootScope.app.config.theme = theme;
	}
        
	
	var ctrl = {};
	$scope.ctrl = ctrl;
	$scope.addTheme = addTheme;
	$scope.deleteTheme = deleteTheme;
	$scope.selectTheme = selectTheme;
	$scope.applyTheme = applyTheme;
});



/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeTheme')
/**
 * @ngdoc directives
 * @name amh-palette-color-chooser
 * @description Directive to show colors of a palette.
 */
.directive('amhPaletteColorChooser', function () {
	return {
		restrict: 'E',
		templateUrl: 'views/directives/amh-palette-color-chooser.html',
		scope: {
			ngModel: '=?',
			amhLabel: '@'
		},
		controller: function ($scope, $mdColorPalette) {
			$scope.colors = Object.keys($mdColorPalette);
		}
	}
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeTheme')
/**
 * @ngdoc directives
 * @name amh-theme-palette-preview
 * @description Directive to show colors of a palette.
 * 
 * Option 'amh-header' determines that a constant toolbar on top of occupied section 
 * by the directive in the view should be visible or not.
 */
.directive('amhThemePalettePreview', function () {
	return {
		restrict: 'E',
		templateUrl: 'views/directives/amh-theme-palette-preview.html',
		scope: {
			primary: '=',
			accent: '='
		},
		controller: function ($scope, $mdColors, $mdColorUtil) {
			$scope.getColor = function (color) {
				return $mdColorUtil.rgbaToHex($mdColors.getThemeColor(color))
			};
		}
	}
})//
.directive('mdJustified', function() {
	return {
		restrict : 'A',
		compile : function(element, attrs)  {
			var layoutDirection = 'layout-'+ (attrs.mdJustified || "row");

			element.removeAttr('md-justified');
			element.addClass(layoutDirection);
			element.addClass("layout-align-space-between-stretch");

			return angular.noop;
		}
	};
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeTheme')
/**
 * Adds basic system settings
 * 
 */
.run(function($preferences) {
	$preferences
	/**
	 * @ngdoc preferences
	 * @name theme
	 * @description Set theme
	 */
	.newPage({
		id: 'theme',
		title: 'Theme Setting',
		templateUrl : 'views/amh-setting/themes.html',
		controller: 'AmhThemeCtrl',
		description: 'Define theme.',
		icon: 'color_lens',
		priority: 3
	})
});

  /*
   * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
   * 
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is
   * furnished to do so, subject to the following conditions:
   * 
   * The above copyright notice and this permission notice shall be included in all
   * copies or substantial portions of the Software.
   * 
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   * SOFTWARE.
   */
  'use strict';
  angular.module('ngMaterialHomeTheme')

          .run(function ($rootScope, $mdTheming) {

              $rootScope.$watch(function () {
                  return $rootScope.app.config.theme;
              }, function (themeName) {
                  if (themeName && $mdTheming.registered(themeName)) {
                      $mdTheming.generateTheme(themeName);
                  }
              });
              // load themes from config
              var themes = $rootScope.app.config.theme;
              if (themes) {
                  themes.forEach(function (theme) {
                      registerTheme(theme);
                  });
              } else {
                  console.log('error');
              }
              
              function registerTheme(theme) {
                  normalize(theme);
                  $mdTheming.defineTheme(theme.name, theme.options);
                  //reload the theme
//		$mdTheming.generateTheme(theme.name);
              }

              function normalize(theme) {
                  // check options
                  if (typeof theme.options === 'undefined') {
                      theme.options = {};
                  } else {
                      if (!theme.options.primary || theme.options.primary === '') {
                          delete theme.options.primary;
                      }
                      if (!theme.options.accent || theme.options.accent === '') {
                          delete theme.options.accent;
                      }
                      if (!theme.options.warn || theme.options.warn === '') {
                          delete theme.options.warn;
                      }
                      if (!theme.options.background || theme.options.background === '') {
                          delete theme.options.background;
                      }
                  }
                  // check name
                  if (typeof theme.name === 'undefined') {
                      theme.name = 'theme' + Math.floor((Math.random() * 10000) + 1);
                  }
              }
          });


 
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeTheme')

/**
 * @ngdoc service
 * @name $theme
 * @description Theme manager
 * 
 * Manages themes of the application
 * 
 */
.service('$theme', function($rootScope, $q, $navigator, $translate) {

//	/**
//	 * Adds new theme
//	 * 
//	 * New theme is added into the themes list.
//	 * 
//	 * @memberof $theme
//	 * @param {string} theme key - Key of the theme
//	 * @return {theme}  registered theme
//	 */
//	function theme(key) {
//		return $rootScope.app.config.themes[key];
//	}

	/**
	 * List of themes
	 * 
	 * @memberof $theme
	 * @return {promise<Array>} of themes
	 */
	function themes() {
		return $q.resolve({
			items : $rootScope.app.config.themes || []
		});
	}

	/**
	 * Adds a new theme
	 * 
	 * @memberof $theme
	 * @return {promise<Array>} of themes
	 */
	function newTheme(theme) {
		if(!$rootScope.app.user.owner){
			return $q.reject('not allowed');
		}
		if(!$rootScope.app.config.themes){
			$rootScope.app.config.themes = [];
		}
		$rootScope.app.config.themes.push(theme);
		return $q.resolve(theme);
	}

	/**
	 * Delete a theme
	 * 
	 * @memberof $theme
	 * @param {object} theme - The theme to delete
	 * @return {promise} of deleted theme
	 */
	function deleteTheme(theme){
		if(!$rootScope.app.user.owner){
			return $q.reject('not allowed');
		}

	    var index = $rootScope.app.config.themes.indexOf(theme);
	    if (index !== -1) {
	    	$rootScope.app.config.themes.splice(index, 1);
	    	return $q.resolve(theme);
	    } else {
	    	return $q.reject('Not found');
	    }
	}

	/**
	 * Returns the name of theme that is currently loaded asynchronously.
	 * 
	 * @memberof $theme
	 * @return {string} name of theme
	 */
	function proposedTheme() {
		return $rootScope.app.config.theme;
	}

	/*
	 * Service struct
	 */
	return {
//		theme : theme,
		themes : themes,
		newTheme : newTheme,
		deleteTheme: deleteTheme,
		proposedTheme : proposedTheme,
	};
});
angular.module('ngMaterialHomeTheme').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amh-setting/theme-manager.html',
    "<md-sidenav class=md-sidenav-left md-component-id=theme-manager-left md-is-locked-open=true> <md-toolbar> <div class=md-toolbar-tools> <label flex translate>Themes</label> <md-button ng-href=preferences/theme class=md-icon-button aria-label=\"Go to theme setting\"> <wb-icon>palette</wb-icon> </md-button> <md-button ng-click=addTheme() class=md-icon-button aria-label=Settings> <wb-icon>add</wb-icon> </md-button> </div> </md-toolbar> <md-content> <md-list> <md-list-item ng-repeat=\"theme in app.config.themes\" ng-click=selectTheme(theme)> <p>{{ theme.title}}</p> <wb-icon ng-click=deleteTheme(theme) aria-label=\"Delete theme\" class=\"md-secondary md-hue-3\">delete</wb-icon> </md-list-item> </md-list> </md-content> </md-sidenav> <md-content layout=column layout-padding flex> <div ng-if=!selectedTheme> <p translate>Select a theme</p> <p> Here you can add/remove themes with preferred color palettes.     </p> </div> <div ng-if=selectedTheme>  <md-input-container> <label translate>Name</label> <input ng-model=selectedTheme.name readonly ng-disabled=true> </md-input-container> <md-input-container> <label translate>Title</label> <input ng-model=selectedTheme.title> </md-input-container> </div> <div ng-if=selectedTheme layout-gt-xs=row layout=column md-theme=default> <div layout=column flex> <md-checkbox ng-model=selectedTheme.options.dark aria-label=Dark> {{'Dark?'| translate}} </md-checkbox> <amh-palette-color-chooser ng-model=selectedTheme.options.primary amh-label=\"{{'Primary Palette'| translate}}\"> </amh-palette-color-chooser> <amh-palette-color-chooser ng-model=selectedTheme.options.accent amh-label=\"{{'Accent Palette'| translate}}\"> </amh-palette-color-chooser> <amh-palette-color-chooser ng-model=selectedTheme.options.warn amh-label=\"{{'Warn Palette'| translate}}\"> </amh-palette-color-chooser> <amh-palette-color-chooser ng-model=selectedTheme.options.background amh-label=\"{{'Background Palette'| translate}}\"> </amh-palette-color-chooser> </div> <div flex class=\"section theme-preview\" layout=row layout-align=\"center center\"> <div layout=column flex=70> <span class=componentTag translate>Theme Preview</span> <amh-theme-palette-preview primary=\"selectedTheme.options.primary || 'indigo'\" accent=\"selectedTheme.options.accent || 'pink'\"> </amh-theme-palette-preview> </div> </div> </div> </md-content>"
  );


  $templateCache.put('views/amh-setting/themes.html',
    "<div layout=column layout-gt-sm=row layout-padding flex> <md-content layout=column flex=none flex-gt-sm=30> <div> <h2 translate>Theme</h2> <p translate> Here you can set your preferred theme for your site. </p> </div> <md-input-container> <label>Theme</label> <md-select ng-model=myTheme ng-init=\"myTheme = app.config.theme\"> <md-option ng-repeat=\"item in themes\" ng-value=item.name> {{item.name|| item.title}} </md-option> </md-select> </md-input-container> <div layout=row> <md-button class=\"md-raised md-primary\" ng-click=applyTheme(myTheme) translate>Apply</md-button> <md-button class=md-raised ng-href=preferences/theme/manager>Theme manager</md-button> </div> </md-content> <div layout=column flex=none flex-gt-sm>  <div> <h2 translate>Preview</h2> </div> <md-content md-whiteframe=5 flex md-theme=\"myTheme || app.config.theme\"> <wb-group ng-model=ctrl.template style=\"border: solid 1px\" md-colors=\"{borderColor:'default-primary-100'}\"> </wb-group> </md-content> </div> </div>"
  );


  $templateCache.put('views/directives/amh-palette-color-chooser.html',
    "<div md-theme=default> <md-input-container class=md-block md-colors=\"{backgroundColor: '{{ngModel}}'}\"> <label md-colors=\"{color: '{{ngModel}}'}\">{{amhLabel}}</label> <md-select ng-model=ngModel> <md-option ng-value=\"\">Default</md-option> <md-option ng-repeat=\"color in colors\" md-colors=\"{backgroundColor: '{{color}}'}\" ng-value=color> {{color}} </md-option> </md-select> </md-input-container> </div>"
  );


  $templateCache.put('views/directives/amh-theme-palette-preview.html',
    "<div layout=column md-theme=default> <div md-colors=\"{background: '{{primary}}-500'}\" md-justified=column class=\"primary line\"> <span>Primary - {{primary}}</span> <div md-justified> <span>500</span> <span>{{getColor(primary + '-500')}}</span> </div> </div> <div md-colors=\"{background: '{{primary}}-700'}\" md-justified class=line> <span>700</span> <span>{{getColor(primary + '-700')}}</span> </div> <div md-colors=\"{background: '{{primary}}-800'}\" md-justified class=line> <span>800</span> <span>{{getColor(primary + '-800')}}</span> </div> <div md-colors=\"{background: '{{accent}}-A200'}\" md-justified=column class=\"accent line\"> <span>Accent - {{accent}}</span> <div md-justified> <span>A200</span> <span>{{getColor(accent + '-A200')}}</span> </div> </div> </div>"
  );

}]);
