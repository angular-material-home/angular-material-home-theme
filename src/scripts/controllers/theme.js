/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeTheme')

/**
 * @ngdoc controller
 * @name AmhThemeCtrl
 * @description Theme setting controller
 * 
 * This controller is used to set theme for SPA
 */
.controller('AmhThemeCtrl', function ($scope, $mdTheming, $rootScope, $http, $theme){

	$scope.themes = $mdTheming.THEMES;
	$scope.myTheme = '';
	
	$http.get('resources/theme-preview.json')//
	.then(function(res){
		$scope.ctrl.template = res.data;
	});
	
	/**
	 * Adds new theme to app configuration
	 * 
	 * @memberof AmhThemeCtrl
	 * @return {promise} of added theme
	 */
	function addTheme(){
		return $theme.newTheme({
			name: 'theme' + Math.floor((Math.random() * 10000) + 1),
			title: 'Theme title',
			options: {}
		});
	}
	
	/**
	 * Remove theme form application
	 * 
	 * @memberof AmhThemeCtrl
	 * @param {object} theme - The theme
	 * @return {promise} to delete theme
	 */
	function deleteTheme(theme){
		return $theme.deleteTheme(theme)
		.then(function(){
			if(angular.equals($scope.selectedTheme, theme)){
				$scope.selectedTheme = null;
			}
		});
	}
	
	/**
	 * Select theme
	 * 
	 * @memberof AmhThemeCtrl
	 * @param {object} theme - The theme
	 */
	function selectTheme(theme){
		$scope.selectedTheme = theme;
		if(!theme.options){
			theme.options = [];
		}
	}
	
	// Watch on list of themes in config
	$scope.$watch(function(){
		return $rootScope.app.config.themes;
	}, function(){
		$rootScope.themes = [];
		// add customer themes
		if($rootScope.app.config.themes){			
			$rootScope.app.config.themes.forEach(function(theme){
				registerTheme(theme);
			});
		}
		$scope.themes = $mdTheming.THEMES;
	}, true);
	
	// Watch on myTheme variable
	$scope.$watch(function(){
		return $scope.myTheme;
	}, function(themeName){
		if(themeName && $mdTheming.registered(themeName)){
			$mdTheming.generateTheme(themeName);
		}
	});
	
	function registerTheme(theme){
		normalize(theme);
		$mdTheming.defineTheme(theme.name, theme.options);
		//reload the theme
//		$mdTheming.generateTheme(theme.name);
	}
	
	function normalize(theme){
		// check options
		if(typeof theme.options === 'undefined'){
			theme.options = {};
		}else{
			if(!theme.options.primary || theme.options.primary === ''){
				delete theme.options.primary;
			}
			if(!theme.options.accent || theme.options.accent === ''){
				delete theme.options.accent;
			}
			if(!theme.options.warn || theme.options.warn === ''){
				delete theme.options.warn;
			}
			if(!theme.options.background || theme.options.background === ''){
				delete theme.options.background;
			}
		}
		// check name
		if(typeof theme.name === 'undefined'){
			theme.name = 'theme' + Math.floor((Math.random() * 10000) + 1)
		}
	}
	
	function applyTheme(theme){
		$rootScope.app.config.theme = theme;
	}
        
	
	var ctrl = {};
	$scope.ctrl = ctrl;
	$scope.addTheme = addTheme;
	$scope.deleteTheme = deleteTheme;
	$scope.selectTheme = selectTheme;
	$scope.applyTheme = applyTheme;
});


