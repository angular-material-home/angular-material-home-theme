/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeTheme')
/*
 * تعیین تم
 */
.config(function($mdThemingProvider) {

//	$mdThemingProvider.generateThemesOnDemand(true);
	$mdThemingProvider.alwaysWatchTheme(true);

	var gPalette = {
			'50' : 'e2f0f4',
			'100' : 'b7d8e3',
			'200' : '88bfd0',
			'300' : '58a5bd',
			'400' : '3491af',
			'500' : '107ea1',
			'600' : '0e7699',
			'700' : '0c6b8f',
			'800' : '096185',
			'900' : '054e74',
			'A100' : 'a3dbff',
			'A200' : '70c7ff',
			'A400' : '3db3ff',
			'A700' : '24a9ff',
			'contrastDefaultColor' : 'light',
			'contrastDarkColors' : [
				'50',
				'100',
				'200',
				'300',
				'A100',
				'A200',
				'A400',
				'A700'
				],
				'contrastLightColors' : [
					'400',
					'500',
					'600',
					'700',
					'800',
					'900'
					]
	};

	var aPalette = {
			'50' : 'fce5e0',
			'100' : 'f8bfb3',
			'200' : 'f49580',
			'300' : 'ef6a4d',
			'400' : 'eb4a26',
			'500' : 'e82a00',
			'600' : 'e52500',
			'700' : 'e21f00',
			'800' : 'de1900',
			'900' : 'd80f00',
			'A100' : 'ffffff',
			'A200' : 'eb4a26',
			'A400' : 'e52500',
			'A700' : 'd80f00',
			'contrastDefaultColor' : 'light',
			'contrastDarkColors' : [
				'50',
				'100',
				'200',
				'300'
				],
				'contrastLightColors' : [
					'400',
					'500',
					'600',
					'700',
					'800',
					'900',
					'A100',
					'A200',
					'A400',
					'A700'
					]
	};

	var bPalette = {
			'50' : 'fdfeff',
			'100' : 'fbfdfe',
			'200' : 'f8fbfe',
			'300' : 'f5f9fe',
			'400' : 'f2f8fd',
			'500' : 'f0f7fd',
			'600' : 'd0e0e3',
			'700' : 'a2c4c9',
			'800' : '76a5af',
			'900' : '45818e',
			'A100' : 'ffffff',
			'A200' : 'ffffff',
			'A400' : 'ffffff',
			'A700' : 'ffffff',
			'contrastDefaultColor' : 'dark',
			'contrastDarkColors' : [
				'50',
				'100',
				'200',
				'300',
				'400',
				'500',
				'600',
				'700',
				'A100',
				'A200',
				'A400',
				'A700'
				],
				'contrastLightColors' : [
					'800',
					'900'
					]
	};

	$mdThemingProvider.definePalette('hitechPrimary', gPalette);
	$mdThemingProvider.definePalette('hitechAccent', aPalette);
	$mdThemingProvider.definePalette('hitechBackground', bPalette);

	$mdThemingProvider.theme('hitech').primaryPalette('hitechPrimary');
	$mdThemingProvider.theme('hitech').accentPalette('hitechAccent');
	$mdThemingProvider.theme('hitech').backgroundPalette('hitechBackground');

	var pPalette = {
		  '50': 'f0e0e0',
		  '100': 'd9b3b3',
		  '200': 'c08080',
		  '300': 'a74d4d',
		  '400': '942626',
		  '500': '810000',
		  '600': '790000',
		  '700': '6e0000',
		  '800': '640000',
		  '900': '510000',
		  'A100': 'ff8484',
		  'A200': 'ff5151',
		  'A400': 'ff1e1e',
		  'A700': 'ff0404',
		  'contrastDefaultColor': 'light',
		  'contrastDarkColors': [
		    '50',
		    '100',
		    '200',
		    'A100',
		    'A200'
		  ],
		  'contrastLightColors': [
		    '300',
		    '400',
		    '500',
		    '600',
		    '700',
		    '800',
		    '900',
		    'A400',
		    'A700'
		  ]
		};
	
	$mdThemingProvider.definePalette('petroPrimary', pPalette);
	
	$mdThemingProvider.theme('petronomics').primaryPalette('petroPrimary');
	$mdThemingProvider.theme('petronomics').accentPalette('indigo');
	
	
//	$mdThemingProvider.setDefaultTheme('hitech');

});