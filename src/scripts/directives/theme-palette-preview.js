/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeTheme')
/**
 * @ngdoc directives
 * @name amh-theme-palette-preview
 * @description Directive to show colors of a palette.
 * 
 * Option 'amh-header' determines that a constant toolbar on top of occupied section 
 * by the directive in the view should be visible or not.
 */
.directive('amhThemePalettePreview', function () {
	return {
		restrict: 'E',
		templateUrl: 'views/directives/amh-theme-palette-preview.html',
		scope: {
			primary: '=',
			accent: '='
		},
		controller: function ($scope, $mdColors, $mdColorUtil) {
			$scope.getColor = function (color) {
				return $mdColorUtil.rgbaToHex($mdColors.getThemeColor(color))
			};
		}
	}
})//
.directive('mdJustified', function() {
	return {
		restrict : 'A',
		compile : function(element, attrs)  {
			var layoutDirection = 'layout-'+ (attrs.mdJustified || "row");

			element.removeAttr('md-justified');
			element.addClass(layoutDirection);
			element.addClass("layout-align-space-between-stretch");

			return angular.noop;
		}
	};
});