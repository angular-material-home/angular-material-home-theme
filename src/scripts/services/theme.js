/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeTheme')

/**
 * @ngdoc service
 * @name $theme
 * @description Theme manager
 * 
 * Manages themes of the application
 * 
 */
.service('$theme', function($rootScope, $q, $navigator, $translate) {

//	/**
//	 * Adds new theme
//	 * 
//	 * New theme is added into the themes list.
//	 * 
//	 * @memberof $theme
//	 * @param {string} theme key - Key of the theme
//	 * @return {theme}  registered theme
//	 */
//	function theme(key) {
//		return $rootScope.app.config.themes[key];
//	}

	/**
	 * List of themes
	 * 
	 * @memberof $theme
	 * @return {promise<Array>} of themes
	 */
	function themes() {
		return $q.resolve({
			items : $rootScope.app.config.themes || []
		});
	}

	/**
	 * Adds a new theme
	 * 
	 * @memberof $theme
	 * @return {promise<Array>} of themes
	 */
	function newTheme(theme) {
		if(!$rootScope.app.user.owner){
			return $q.reject('not allowed');
		}
		if(!$rootScope.app.config.themes){
			$rootScope.app.config.themes = [];
		}
		$rootScope.app.config.themes.push(theme);
		return $q.resolve(theme);
	}

	/**
	 * Delete a theme
	 * 
	 * @memberof $theme
	 * @param {object} theme - The theme to delete
	 * @return {promise} of deleted theme
	 */
	function deleteTheme(theme){
		if(!$rootScope.app.user.owner){
			return $q.reject('not allowed');
		}

	    var index = $rootScope.app.config.themes.indexOf(theme);
	    if (index !== -1) {
	    	$rootScope.app.config.themes.splice(index, 1);
	    	return $q.resolve(theme);
	    } else {
	    	return $q.reject('Not found');
	    }
	}

	/**
	 * Returns the name of theme that is currently loaded asynchronously.
	 * 
	 * @memberof $theme
	 * @return {string} name of theme
	 */
	function proposedTheme() {
		return $rootScope.app.config.theme;
	}

	/*
	 * Service struct
	 */
	return {
//		theme : theme,
		themes : themes,
		newTheme : newTheme,
		deleteTheme: deleteTheme,
		proposedTheme : proposedTheme,
	};
});