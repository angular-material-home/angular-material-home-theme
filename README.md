# AMH Theme

[![pipeline status](https://gitlab.com/angular-material-home/angular-material-home-theme/badges/master/pipeline.svg)](https://gitlab.com/angular-material-home/angular-material-home-theme/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/ff9075d87b614cf5b1cbba89c005bb3e)](https://www.codacy.com/app/angular-material-home/angular-material-home-theme?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-home/angular-material-home-theme&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/ff9075d87b614cf5b1cbba89c005bb3e)](https://www.codacy.com/app/angular-material-home/angular-material-home-theme?utm_source=gitlab.com&utm_medium=referral&utm_content=angular-material-home/angular-material-home-theme&utm_campaign=Badge_Coverage)

Adds features to manage themes.

## Install

### Manually

Download content of 'dist' directory of project and place files in your project.

### Bower

Use following command:

	bower install --save angular-material-home-theme

### Yeoman

## Development

We are using following tools to develop this module. Before starting development you should install below tools.

- nodejs
- npm
- grunt-cli
- bower

### Preparing to develop

Get source of project:

	git clone https://gitlab.com/angular-material-home/angular-material-home-theme.git
	
Now run following commands respectively in root directory of project.

	npm install
	bower install

### Demo 

Run following command to run demo:

	grunt demo

	