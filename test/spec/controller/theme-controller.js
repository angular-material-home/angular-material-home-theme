'use strict';

describe('AmhThemeCtrl', function() {

	// load the controller's module
	beforeEach(module('ngMaterialHomeTheme'));

	var ContentCtrl, scope;
	var translate;
	var rootScope = {
		$on : function() {
		}
	};

	// Initialize the controller and a mock scope
	beforeEach(inject(function($controller, $rootScope, _$cms_) {

		scope = $rootScope.$new();
		ContentCtrl = $controller('AmhThemeCtrl', {
			$scope : scope,
			// place here mocked dependencies
			$translate : translate,
			$rootScope : rootScope
		});
	}));

	it('should attach a list of awesomeThings to the scope', function() {
		// expect(ContentCtrl.awesomeThings.length).toBe(3);
	});
});
